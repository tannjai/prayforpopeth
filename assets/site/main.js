$(document).ready(function() {

$("img").one("load",function(){
$('.cards').masonry({
      itemSelector: '.card'
    });
});
$('.cards').masonry({
    itemSelector: '.card'
  });
 $('.special.card .image').dimmer({
  on: 'hover'
});
 $('.star.rating').rating();
 $('.card .dimmer').dimmer({
  on: 'hover'
});
  

      // fix main menu to page on passing
      $('.main.menu').visibility({
        type: 'fixed'
      });
      $('.overlay').visibility({
        type: 'fixed',
        offset: 80
      });

      // lazy load images
      $('.image').visibility({
        type: 'image',
        transition: 'vertical flip in',
        duration: 500
      });

      // show dropdown on hover
      $('.main.menu  .ui.dropdown').dropdown({
        on: 'hover'
      });
      $('.show-modal').click(function(){
        var card = $(this).parents('.card');
        var upload = $(card).find('.upload-image');
        var user = $(card).find('.user-name');
        var date = $(card).find('.date').text();
        var name = $(user).text();
        var upload_src = $(upload).attr('src');
        var description = $(card).find('.description');
        var description_text = $(description).text();
        $('#modal-image').attr('src',upload_src);
        $('#modal-desc').text(description_text);
        $('#modal-user').text(name);
        $('#modal-date').text(date);
        $('.ui.modal').modal('show');
     });
    });