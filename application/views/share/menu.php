<!-- MENU-->
<div class="ui borderless stackable main menu">
  <div class="ui text container">     
    <a href="<?php echo base_url('post')?>" class="item">Send Prays</a>
    <a href="<?php echo base_url('read')?>" class="item">Read Prays</a>
    <a href="<?php echo base_url()?>" class="header item">
      <img class="logo" src="<?php echo base_url('assets/images/logo.jpg')?>">        
    </a> 
    <a href="<?php echo base_url('content')?>" class="item">Pope Francis</a>
    <a href="http://www.facebook.com/prayforpopeTH" target="blank" class="item">Contact Us</a>    
  </div>
</div> 

<nav class="navbar navbar-default fixed">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     <a href="<?php echo base_url()?>" class="header item">
      <img class="logo" src="<?php echo base_url('assets/images/logo.jpg')?>" style="position: relative;
    right: -30px;">        
    </a> 
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo base_url('post')?>">Send Prays</a></li>        
        <li><a href="<?php echo base_url('read')?>">Read Prays</a></li>        
        <li><a href="<?php echo base_url()?>">Pope Francis</a></li>        
        <li><a href="http://www.facebook.com/prayforpopeTH" target="blank">Contact Us</a></li>        
      </ul>            
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>