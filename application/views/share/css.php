<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  
  <!-- Site Properities -->
  <title>PrayforpopeTH</title>

  

  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendors/bootstrap/css/bootstrap.min.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dist/jquery.fullpage.css')?>" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/statistic.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/reset.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/checkbox.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/site.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/container.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/grid.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/header.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/image.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/menu.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/divider.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/card.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/list.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/segment.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/dropdown.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/icon.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/transition.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/button.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/label.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/reveal.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/dimmer.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/rating.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/popup.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/modal.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/sidebar.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/form.css');?>">  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/main.css');?>">
  <meta property="og:title" content="<?php if(isset($post->name)) echo $post->name.'-PrayforpopeTH'; else echo 'PrayforpopeTH'; ?>">
  <meta property="og:description" content="<?php if(isset($post->message)) echo $post->message; else echo 'PrayforpopeTH'; ?>">
  <?php 
  if(isset($post->image)&&$post->image!=''&&$post->image!='false'){
    $image = json_decode($post->image);
  ?>
  <meta property="og:image"
  content="<?php echo base_url('assets/uploads/'.$image[0])?>" />
  <?php }else{ ?>
  <meta property="og:image" content="<?php echo base_url('assets/images/main.jpg')?>" />
  <?php } ?>
</head>