<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properities -->
  <title>PrayforpopeTH</title>

  

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendors/bootstrap/css/bootstrap.min.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dist/jquery.fullpage.css')?>" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/statistic.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/reset.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/checkbox.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/site.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/container.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/grid.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/header.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/image.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/menu.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/divider.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/card.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/list.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/segment.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/dropdown.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/icon.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/transition.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/button.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/label.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/reveal.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/dimmer.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/rating.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/popup.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/modal.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/sidebar.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/components/form.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/site/main.css');?>">



  <style>
  /* Style for our header texts
  * --------------------------------------- */
  h1,h2,h3,h4,h5,h6{
   font-family: 'Lato', 'Helvetica Neue', sarabun, Helvetica, sans-serif;
}
  h1{
    font-size: 5em;
    margin:0;
    color: #494D49;
  }
   h2{
    font-size: 3em;
    margin:0;
    color: #494D49;
    padding-bottom: 20px;
  }
  h3{
    font-size: 2em;
  }
  h4{
    font-size: 1.7em;
  }
  .intro p{
    color: #494D49;
    text-align: justify;
    font-family: 'Lato', 'Helvetica Neue', sarabun, Helvetica, sans-serif;
    font-size: 1.7em;
    line-height: 1.3;
  }
  .intro a{
        color: #1C428E;
    font-weight: bold;
    text-decoration: none;
  }
  .intro{
    padding-left: 60px;
    padding-right: 60px;
    padding-top: 60px;
  }
  #pope{
    font-size: 1.5em;
  }
  .name-th{
    font-family: 'Lato', 'Helvetica Neue', sarabun, Helvetica, sans-serif;
    font-size:2em;
    padding-top: 10px;
  }
  .name-en{
    font-family: 'Lato', 'Helvetica Neue', sarabun, Helvetica, sans-serif;
    font-size:1.5em;
    padding-top: 10px;
  }
  .text-right{
    float:right;
  }
  .facebook{
    padding-top: 30px;
    line-height: 1;
    color: #3b5998;
  }
   .facebook-text{
    color: #494D49;
    margin-top: 30px;
  }
  /* Centered texts in each section
  * --------------------------------------- */
  .section{
    text-align:center;
    border-bottom: 5px solid white;
  }
  .section0{
    text-align:center;
  }
  .fb_iframe_widget iframe{
    padding: 5px;
    background-color: white;
  }
  .section0 h1 i{
    font-size: 40px;
    position: relative;
    top: -30px;
  }
  #section0{
    background-image: url('assets/images/pope-banner.jpg');
    background-size: cover;
  }
  #section1,#section2{
    background-image: url('assets/images/banner2.jpg');
    background-size: cover;
  }

  #header{
    position:fixed;
    height: 50px;
    display:block;
    width: 100%;
    background: #333;
    z-index:9;
    text-align:center;
    color: #f2f2f2;
    
  }

  #header{
    top:0px;
  }
  
</style>

  <!--[if IE]>
    <script type="text/javascript">
       var console = { log: function() {} };
    </script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url('assets/vendors/jquery.slimscroll.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/dist/jquery.fullpage.js')?>"></script>

    <script type="text/javascript">
      $(document).ready(function(){
        $("#demosMenu").change(function(){
          window.location.href = $(this).find("option:selected").attr("id") + '.html';
        });
      });
      $(document).ready(function() {
        $('#fullpage').fullpage({
          sectionsColor: ['#f5f5f5', '#f5f5f5', '#f5f5f5', '#f5f5f5', '#f5f5f5'],
          navigation: true,
          navigationPosition: 'right',
          responsiveWidth: 900,
          slidesNavigation: true,
          setAutoScrolling: true,
        });
      });
    </script>

  </head>
  <body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=873869926044741";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <div id="header">
      <?php include('menu.php'); ?>
    </div>