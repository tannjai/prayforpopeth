<?php 
$this->load->view('share/header-index.php');
?>
<div id="fullpage">
	<div class="section0 section" id="section0">
		<div class="intro intro1">
			<h1>
				PRAY FOR ME,
			</h1>
			<h3>and if some of you can't pray because you are not believers, send me good vibrations.<br/></h3>
			<h3>ภาวนาให้พ่อด้วย และถ้าบางคนสวดไม่ได้ เพราะไม่ใช่คริสตชน<br/>
				ก็ขอให้ส่งแรงใจมาให้พ่อ</h3>
				<div class="name-en text-right" id="pope">-POPE FRANCIS-</div><br/><br/>
				<a href="<?php echo base_url('post')?>" class="ui massive button color">Get Started <i class="right arrow icon"></i></a>
			</div>
		</div>
		<div class="section1 section" id="section1">
			<div class="slide" id="slide1">
				<div class="ui container">
					<div class="ui column stackable grid ">
						<div class="six wide column">
							<img src="<?php echo base_url()?>assets/images/avatar/steve.jpg" class="full-img max-img">
						</div>
						<div class="ten wide column">
							<div class="intro">
								<h2>"ไม่ว่าพระสันตะปาปาเสด็จไปที่ใด<br/> พระองค์มักทรงขอให้ทุกคนภาวนาให้พระองค์"</h2>
								<p>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									ง่าย ๆ เลย ไม่ต้องคิดอะไรมาก เพราะพระสันตะปาปาคือพ่อของเรา เมื่อเรารักพ่อ เราก็ต้องสวดภาวนาให้พ่อ ขอให้ท่านมีสุขภาพที่แข็งแรง เหมือนที่เราปรารถนาสำหรับพ่อของเรา
									เมื่อพระสันตะปาปาทรงเอาพระทัยใส่คนเล็ก ๆ ในสังคมอยู่เสมอ เราที่เป็นคนเล็ก ๆ จึงต้องสวดภาวนาเพื่อพระองค์ เพื่อวอนขอพระเจ้าโปรดทรงพิทักษ์รักษาพระองค์ท่านในพระภารกิจการเป็นผู้นำและผู้รับใช้ประชากรของพระเจ้า <a href="<?php echo base_url('content')?>">...อ่านต่อ</a>
								</p>
								<div class="text-right">
									<div class="name-th">มาเซอร์มารี หลุยส์ พรฤกษ์งาม</div>
									<div class="name-th">ผู้แปลหนังสือ พระสมณสาสน์ พระสันตะปาปา</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="slide" id="slide2">
				<div class="intro ui container">

					<div class="ui column stackable grid ">

						<div class="ten wide column">
							<div class="intro">
								<h2>WHY WE SHOULD PRAYFORPOPE?</h2>
								<p>PRAYFORPOPE THAILAND คือเว็บไซต์ที่เชิญชวนให้มาร่วมสวดภาวนาให้กับพระสันตะปาปา 
									ไม่ว่าจะผ่านทางการสวดสายประคำ / การภาวนาเป็นคำพูด / หรือผ่านทางภาพวาด เพื่อเป็นพละกำลัง และกำลังใจให้กับพระสันตะปาปาฟรังซิสในการดำเนินภารกิจของพระเป็นเจ้าของท่าน</p>
									<div class="text-right">
										<div class="name-th">ผู้จัดทำ</div>
										<div class="name-en" >
											<a href="https://www.facebook.com/prayforpopeTH">
												<img src="<?php echo base_url()?>assets/images/logo.jpg" style="height: 40px;">
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="six wide column">
								<img src="<?php echo base_url()?>assets/images/avatar/steve.jpg" class="full-img max-img">
							</div>
						</div>
					</div>
				</div>
			</div>
	<!-- <div class="slide" id="slide2">
			<div class="intro ui container">
	
				<div class="ui column stackable grid ">
	
					<div class="ten wide column">
						<div class="intro">
							<h2>ทำไมเราต้อง PRAYFORPOPE?</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, cumque tempore voluptate minima. Voluptate, optio. Voluptates ut quas necessitatibus facere fugiat quo, soluta tenetur, ex ducimus dicta omnis eos debitis.</p>
							<div class="name-en">P'Win</div>
							<div class="name-en">Admin Page Pope Report</div>
						</div>
					</div>
					<div class="six wide column">
						<img src="<?php echo base_url()?>assets/images/avatar/steve.jpg" class="full-img max-img">
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<div class="section2 section" id="section2">
		<div class="intro ui column stackable grid">
			<div class="two wide column"></div>
			<div class="four wide column">
				<div class="img">
					<img src="<?php echo base_url()?>assets/images/rosary.jpg" style="height:200px;">
				</div>
				<h4>Rosary<br/> สวดสายประคำ</h4>
				<p>การภาวนาผ่านทางสายประคำ เป็นการภาวนาผ่านทางแม่พระ เพื่อวอนขอพระพรให้กับพระสันตะปาปา</p>
			</div>
			<div class="four wide column">
				<div class="img">
					<img src="<?php echo base_url()?>assets/images/msg.jpg" style="height:200px;">
				</div>
				<h4>Message<br/> คำภาวนา</h4>
				<p>ข้อความ หรือ คำภาวนา ที่ต้องการจะส่งให้กับพระสันตะปาปา 
					บางครั้งเราก็มีคำพูดที่นอกเหนือจากคำภาวนาตามที่พระศาสนจักรกำหนด
					เราสามารถพิมพ์ข้อความเหล่านั้นที่นี่</p>
				</div>
				<div class="four wide column">
					<div class="img">
						<img src="<?php echo base_url()?>assets/images/pic.jpg" style="height:200px;">
					</div> 
					<h4>Picture<br/> รูปภาพ</h4>
					<p>บางครั้งรูปภาพ ก็สามารถสื่อความหมายได้ดีกว่า ตัวอักษร
						หรือหากท่านมีภาพบางภาพที่แทนคำภาวนาให้กับพระสันตะปาปา
						ก็สามารถส่งเข้ามาผ่านทางช่องทางนี้ได้</p>
					</div>
					<div class="two wide column"></div>
				</div>
			</div>
			<div class= 	"section2 section" id="section2">
				<div class="intro ui container">
					<div class="ui equal width grid stackable ex">
						<div class="column">
							<h1 style="color: #A51C1C; padding-bottom:20px;">EXAMPLE</h1>
							<a href="<?php echo base_url('read')?>">
								<img class="example" src="<?php echo base_url()?>assets/images/ex1.png">
							</a>
						</div>
						<div class="column">
							<a href="<?php echo base_url('read')?>">
								<img class="example" src="<?php echo base_url()?>assets/images/ex2.png">
							</a>
						</div>
						<div class="column">
							<a href="<?php echo base_url('read')?>">
								<img class="example" src="<?php echo base_url()?>assets/images/ex3.png">
								<a href="<?php echo base_url('read')?>" class="ui massive button red ex">
									Read more  
									<i class="plus icon"></i></a>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="section2 section" id="section2">
					<div class="intro ui container">
						<div class="ui column stackable grid ">
							<div class="seven wide column">
								<div class="facebook">
									<a target="_blank" href="http://www.facebook.com/prayforpopeTH">
										<i class="facebook square icon" style="font-size:150px;"></i>
										<h2 class="facebook-text">prayforpopeTH</h2></a>
									</div>
								</div>
								<div class="two wide column"></div>
								<div class="seven wide column">
									<div class="fb-page" data-href="https://www.facebook.com/prayforpopeTH" data-width="500" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true">
										<div class="fb-xfbml-parse-ignore">
											<blockquote cite="https://www.facebook.com/prayforpopeTH">
												<a href="https://www.facebook.com/prayforpopeTH">PrayforpopeThailand</a>
											</blockquote>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</body>
			<script src="<?php echo base_url('assets/vendors/bootstrap/js/bootstrap.min.js')?>"></script>
			</html>