<?php include('share/header.php'); ?>
<style>
  #show{
    position: relative !important;
    left: 50% !important;
    margin-left: -173px !important;
  }
</style>
<body>
<section id="main-content">
  <div class="ui container">
    <?php
    if($count>0){
      ?>
      <section class="box">
        <div class="ui stackable three cards ">
          <div class="ui card" id="show">
            <div class="content">
              <div class="header">
                <span class="user-name">
                  <?php 
                  echo $post->name; 
                  $first_char = mb_substr ($post->name, 0,1);
                  ?>     
                </span>         
                <div class="circle color" 
                <?php if($post->bg!=''){ ?>
                style="background-color:<?php echo $post->bg?> !important;"
                <?php } ?>                
                >
                <?php echo strtoupper($first_char); ?>
              </div>
            </div>
            <div class="meta">
              <span class="date">
                <?php
                $datestring = "%d %M %Y";
                $time = strtotime($post->created_at);                
                echo mdate($datestring, $time);
                ?>
              </span>
            </div>
          </div>
          <?php if($post->image!=''&&$post->image!='false'){ ?>
          <div class="blurring dimmable image">
            <div class="ui dimmer show-modal">

            </div>
            <?php
            $image = json_decode($post->image);
            ?>
            <img class="upload-image" src="<?php echo base_url('assets/uploads/'.$image[0])?>">
          </div>
          <?php } ?>
          <div class="content">
           <div class="description">
            <?php echo $post->message; ?>
          </div>
        <?php if($post->rosary!=0){ ?>
        <div style="text-align:right;">
            <img src="<?php echo base_url('assets/images/rosaryicon.png')?>" height="15" style="position:relative; top:-3px;">
            <?php 
                  echo $post->rosary; 
                  ?>     
        </div>
        <?php } ?>
        </div>
      </div>

    </div>
  </section>
  <?php } ?>
     <div class="but center" style="border=1">
     <p> ร่วมเป็นส่วนหนึ่งในการส่งคำภาวนาให้กับพระสันตะปาปา ได้ที่นี่
     </p>
      <a href="<?php echo base_url('post')?>" class="ui massive button color" 
                <?php if($post->bg!=''){ ?>
                style="background-color:<?php echo $post->bg?> !important;color:#fff;"
                <?php } ?>>
      Send my pray <i class="right arrow icon"></i></a>
    </div>
</section>




<?php include('share/footer.php'); ?>