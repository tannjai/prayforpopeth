<?php include('share/header.php');?>
<style>
  body{
    background-color: #f5f5f5;
  }
</style>
<section id="main-content">
  <div class="ui container">
    <?php
    if($posts['count']>0){
      ?>
      <section class="box">
        <div class="ui stackable three cards ">
          <?php foreach($posts['result'] as $post){?>        
          <div class="ui card">
            <div class="content">
              <div class="header">
                <span class="user-name">
                  <?php 
                  echo $post->name; 
                  $first_char = mb_substr ($post->name, 0,1);
                  ?>     
                </span>         
                <div class="circle color" 
                <?php if($post->bg!=''){ ?>
                style="background-color:<?php echo $post->bg?> !important;"
                <?php } ?>                
                >
                <?php echo strtoupper($first_char); ?>
              </div>
            </div>
            <div class="meta">
              <span class="date">
                <?php
                $datestring = "%d %M %Y";
                $time = strtotime($post->created_at);                
                echo mdate($datestring, $time);
                ?>               
              </span>
            </div>
          </div>
          <?php if($post->image!=''&&$post->image!='false'){ ?>
          <div class="blurring dimmable image">
            <div class="ui dimmer show-modal">

            </div>
            <?php
            $image = json_decode($post->image);
            ?>
            <img class="upload-image" src="<?php echo base_url('assets/uploads/'.$image[0])?>">
          </div>
          <?php } ?>
          <div class="content">
            <div class="description">
              <?php echo $post->message; ?>
            </div>
          </div>
          <?php if($post->rosary!=0){ ?>
          <div class="content">
            <img src="<?php echo base_url('assets/images/rosaryicon.png')?>" height="15" style="position:relative; top:-3px;">
            <?php 
            echo $post->rosary; 
            ?>
            <div class="ui icon button mini share" style="float:right;background-color:#3b5998; color:white;font-size:12px;padding:5px;">
              <a target="_blank" href="http://www.facebook.com/share.php?message=test&u=http://prayforpope.begin.in.th/read/<?php echo $post->id; ?>"><i class="facebook icon square"></i> share</a>
            </div>  
          </div>
          <?php } ?>
      </div>
      <?php } ?>      
    </div>
  </section>
  <?php } ?>

  <!-- Modal -->
  <div class="ui modal" id="my-modal">
    <i class="close icon"></i>    
    <div class="content">
      <div class="ui image">
       <img id="modal-image" src="assets/images/avatar/nan.jpg" width="100%">
     </div>
     <div class="modal-desc description">
      <div class="ui header">
        <h3 id="modal-user">Tann Hiranyawech</h3>
      </div>
      <div id="modal-date"></div>
      <p id="modal-desc">
        dsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasddsafsadfasd
        dsafsadfasd
      </p>
    </div>
  </div>
</div><!--END CONTAINER-->
</section>




<?php include('share/footer.php'); ?>