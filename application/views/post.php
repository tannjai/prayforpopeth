<?php include('share/header.php'); ?>
<style>
  body{
    background-image: url('/assets/images/banner2.jpg');
    background-size: cover;
  }
  #file-alt{
    display: none;
  }
</style>
<div class="ui vertical masthead segment" id="post-page">
<div class="ui container stackable">
    <section>
      <div class="ui card form" id="contact-card" > 
        <form action="<?php echo base_url('post/form')?>" method="post" enctype="multipart/form-data">
          <h1 class="ui center aligned icon header">
            PRAY FOR POPE</h1>
            <div class="post-desc">
              มาร่วมภาวนาเพื่อพระสันตะปาปาฟรังซิส <br/>
              เพื่อเป็นพลังในการทำงานของพระเป็นเจ้าของท่าน <br/>
              ผ่านทาง การสวดสายประคำ / ข้อความและคำภาวนา / รูปภาพ <br/>
            </div>
            <div class="ui clearing divider"></div>
            <div class="fields">
              <div class="field required eight wide">
                <label>Name / ชื่อ</label>
                <input type="text" name="name" placeholder="Name">
              </div>
              <div class="field eight wide">
                <label>Email / อีเมล์</label>
                <input type="email" name="email" placeholder="Email">
              </div>
            </div>
            <div class="fields">
              <div class="field eight wide">
                <label>Rosary / จำนวนสายประคำ</label>
                <input type="number" name="rosary" placeholder="0" max="500" min="0">
              </div>              
            </div>
            <div class="required field">
              <label>Message to pope / ข้อความ</label>
              <textarea rows="5" width="100%" name="message" placeholder="Message"></textarea>
            </div>
            <div class="field">
              <div id="file" class="ui icon button left floated " style="background-color:#494D49; color:white">
                <i class="photo icon"></i> UPLOAD
              </div><span id="file-name"></span>
              <input type="file" id="file-alt" name="image">
              <button class="ui right floated button primary">SEND</button>
            </div>
          </div>
        </form>
      </section>
    </div>
  </div>
</div><!--END CONTAINER-->
<?php include('share/footer.php'); ?>
<script>
  $(document).ready(function(){
    $('#file').click(function(){
      $('#file-alt').click();
    });
    $('#file-alt').change(function(){
      var file_name = $(this).val();
      y             = file_name.split('\\');
      $('#file-name').text($(y).get(-1));
    });
  });
</script>