<?php
	class Post_model extends CI_Model {
		
		public $data;
		public $setting;

		public function __construct()
        {
            parent::__construct();
			$this->setting['table_name']          = 'posts';
			$this->setting['table_name_for_like'] = 'like_post';
        }

		public function insert($data)
		{
			$this->db->insert($this->setting['table_name'], $data);
		}

		public function like($post_id,$status)
		{
			$condition = ['post_id'=>$post_id,'user_id'=>$this->session->userdata('id')];
			if($status==1)
			{			
				$new_data = $condition;
				$this->db->insert($this->setting['table_name_for_like'],$new_data);
				$return['data'] = ['message'=>'update','key'=>$status];
			}else{
				$this->db->where($condition);
				$this->db->delete($this->setting['table_name_for_like']);
				$return['data'] = ['message'=>'delete','key'=>$status];
			}
			$return['status'] = 200;
			return $return;
		}

		function getPosts($per_page=false,$page=false,$condition=false,$condition_like=false)
		{
			// $this->db->order_by('sequence', 'ASC');	
			// $this->db->where('disable', '0');	
			if($condition)
			{
				$this->db->where($condition);
			}
			if($condition_like)
			{
				$this->db->like($condition_like);
			}
			$count_all           = $this->db->count_all_results('posts');
			$result['count_all'] = $count_all;

			// result
			// $this->db->where('disable', '0');	
			$this->db->order_by('created_at', 'DESC');			
			if($condition)
			{
				$this->db->where($condition);
			}	
			if($condition_like)
			{
				$this->db->like($condition_like);
			}
			if($per_page)
			{
				$this->db->limit($per_page,($page-1)*$per_page);		
			}	
			$query = $this->db->get('posts');
			// echo $this->db->last_query();
			$result['result'] = $query->result();
			$result['count'] = $query->num_rows();
			return $result;
		}
	}
?>