<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sp_upload extends CI_Model {

	public function __construct()
    {
    	parent::__construct();        
    }

	public function do_upload($field_name,$path=false)
    {    	       
    	if(!$path)
    	{
	    	$date = date("Ym");    	
			$path  = './assets/uploads/'.$date;
		}
        $config['upload_path']   = $path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name']  = TRUE;                
        $this->load->library('upload',$config);
		if(!is_dir($path))
		{
			 mkdir($path, 0777, TRUE);
		}		
        $images = [];     
        if(!is_array($_FILES[$field_name]['name']))
        {            
        	$_FILES['images']['name']= $_FILES[$field_name]['name'];
            $_FILES['images']['type']= $_FILES[$field_name]['type'];
            $_FILES['images']['tmp_name']= $_FILES[$field_name]['tmp_name'];
            $_FILES['images']['error']= $_FILES[$field_name]['error'];
            $_FILES['images']['size']= $_FILES[$field_name]['size'];
            $fileName = $_FILES[$field_name]['name'];
            if ($this->upload->do_upload('images')) {
                $data = $this->upload->data();
                $images[] = $date.'/'.$data['file_name'];
            } else {            	
                return false;
            }
        }else{
        	$tmp = $_FILES[$field_name]['name'];
        	foreach ($tmp as $key => $image) {	                	
	            $_FILES['images[]']['name']= $_FILES[$field_name]['name'][$key];
	            $_FILES['images[]']['type']= $_FILES[$field_name]['type'][$key];
	            $_FILES['images[]']['tmp_name']= $_FILES[$field_name]['tmp_name'][$key];
	            $_FILES['images[]']['error']= $_FILES[$field_name]['error'][$key];
	            $_FILES['images[]']['size']= $_FILES[$field_name]['size'][$key];
	            $fileName = $_FILES[$field_name]['name'][$key];
	            if ($this->upload->do_upload('images[]')) {
	                $data = $this->upload->data();
	                $images[] = $date.'/'.$data['file_name'];
	            } else {
	                return false;
	            }
	        }
        }        		
        return $images;            
    }	
}
?>