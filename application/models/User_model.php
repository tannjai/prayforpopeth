<?php
	class User_model extends CI_Model {
		
		public $data;
		public $setting;

		public function __construct()
        {
            parent::__construct();
            $this->setting['table_name'] = 'users';
        }

		public function insert($data)
		{
			$this->db->insert($this->setting['table_name'], $data);
		}

		public function getUser($condition = false)
		{
			if($condition)
			{
				$this->db->where($condition);
			}
			$result['query'] = $this->db->get($this->setting['table_name']);
			$result['count'] = $result['query']->num_rows();
			return $result;
		}
	}
?>