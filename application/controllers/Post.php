<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

	public $setting;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Post_model');
		$this->load->model('Sp_upload');
		$this->setting['per_page'] = 10;
	}

	public function index()
	{        
		$this->page();
	}

    public function page($page=1)
    {
        $per_page           = $this->setting['per_page'];
        $return             = $this->Post_model->getPosts($per_page,$page);        
        $this->data['objs'] = $return['result'];                
        $this->load->model('Pagination_model');        
        $this->Pagination_model->setPagination($this->uri->segment(1),$return['count_all'],$this->setting['per_page']);
        // load view 
        // $this->view($this->setting['template'],$this->data);
    }


	public function rule($id=false)
	{
		$this->form_validation->set_rules('message', 'Message', 'required');
	}

	public function form()
	{
		$this->load->library('form_validation');
		$this->rule();
		if ($this->form_validation->run() == FALSE)
        {        	     
            $this->load->view('post');
        }else{        
			$new_data['email']      = $this->input->post('email');
			$new_data['name'] 		= $this->input->post('name');			
			$new_data['message']    = $this->input->post('message');
			$new_data['rosary']     = $this->input->post('rosary');
			if(isset($_FILES['image']))
			{
				$image = $this->Sp_upload->do_upload('image');
				$new_data['image'] = json_encode($image);
			}			
			$arr_bg         = ['#1abc9c','#2ecc71','#3498db','#9b59b6','#f1c40f','#e67e22','#e74c3c'];
			$bg             = array_rand($arr_bg,1);
			$new_data['bg'] = $arr_bg[$bg];
			// save in database
			$this->Post_model->insert($new_data);			
			// redirect
			redirect(base_url('read'),'refresh');
        }        
	}
}
