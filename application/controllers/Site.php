<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	public function index()
	{
		$this->load->view('index');
	}

	public function post()
	{
		$this->load->view('post');
	}

	public function read()
	{
		$this->load->model('Post_model');
		$this->load->helper('date');
		$data['posts'] = $this->Post_model->getPosts();
		$this->load->view('read',$data);	
	}

	public function content()
	{
		$this->load->view('content');	
	}

	public function show($id=false)
	{
		$this->load->model('Post_model');
		$this->load->helper('date');
		$condition = array('id'=>$id);
		$tmp = $this->Post_model->getPosts(false,false,$condition);
		if($tmp['count']==0)
		{
			redirect(base_url(),'refresh');
		}else{
			$data['post'] = $tmp['result'][0];		
			$data['count'] = $tmp['count'];
			$this->load->view('show',$data);
		}		
	}
}
